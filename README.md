# PLEASE READ BEFORE INSTALL #


### What is this repository for? ###

* This is automation test coverage for regression suite. It is fully integrated with TM4J and all results will appear in test cycle of existing project.
* Version 1.0

### How do I get set up? ###

* download latest version of Python https://www.python.org/downloads/
* clone this repository to your local machine
* install all packages from requirements.txt
* run ```pip install -U pytest``` in terminal to install pytest
* run ```brew cask install chromedriver``` in terminal to install chromedriver
* run ```pytest main/${test}``` in terminal to create new Test Cycle (needed for 1st run/updating, all subsequent results will save to existing Test Cycle before updating)
* run ```pytest tests/${test}``` in terminal to get execition result
* give execution permition to chromedriver from 'System Preferences/Security & Privacy' by pressing 'Open Anyway' button if needed
* find latest test run in Jira project to see results


### Who do I talk to? ###

* fell free to ask me any questions 