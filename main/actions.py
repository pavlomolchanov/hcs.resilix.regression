from selenium import webdriver
import yaml
import time
import requests

# Test data
test_data = r'../test_data/preparation.yaml'
configuration = yaml.load(open(test_data), Loader=yaml.FullLoader)
default_user = configuration.get('DEFAULT DATA')['default_user']
default_password = configuration.get('DEFAULT DATA')['default_pass']
driver = webdriver.Chrome()
driver.implicitly_wait(15)
driver.get("https://test.resilix.ai/")


def log_in_default_user():
    username = driver.find_element_by_id("user-email-address")
    password = driver.find_elements_by_class_name("form-control")[1]
    login_button = driver.find_element_by_id("user-")
    time.sleep(5)
    username.send_keys(default_user)
    password.send_keys(default_password)
    login_button.click()
    time.sleep(5)
    but = driver.find_element_by_class_name("text-center.d-inline-flex.align-items-center.justify-content-center.picker-container.btn-account-logo")
    but.click()


def open_patient_list():
    time.sleep(5)
    driver.find_elements_by_class_name("text-center")[4].click()


def select_first_patient():
    time.sleep(5)
    driver.find_elements_by_class_name("vgt-left-align")[6].click() # First patient


def open_optin_and_reminders_tab():
    time.sleep(5)
    driver.find_elements_by_class_name("text-center.nav-link.sharpCorners.navTextentity")[4].click()
    tab_title = driver.find_element_by_class_name("card-title.text-muted.px-3").text
    return tab_title


def email_press_opt_in_out_button():
    time.sleep(5)
    driver.find_elements_by_class_name("btn.btn-danger.btn-danger-custom.ml-3")[1].click()  # Press to OptIn/Out on email


def call_press_opt_in_out_button():
    time.sleep(5)
    driver.find_element_by_xpath("//*[@id='Call Consents']/th[2]/span/div/div[1]/div/div/div/button").click()  # Press to OptIn/Out on calls


def email_button_status():
    time.sleep(5)
    status = driver.find_element_by_xpath("//*[@id='Email Consents']/th[2]/span/div/div[1]/div/div/div/button").text  # button status OptIn/Out on email
    return status


def call_button_status():
    time.sleep(5)
    status = driver.find_element_by_xpath("//*[@id='Call Consents']/th[2]/span/div/div[1]/div/div/div/button").text  # button status OptIn/Out on call
    return status


def press_proceed():
    time.sleep(5)
    driver.find_element_by_xpath("//*[@id='btnPromptModaloptoutBtnconsentwizard1']/div/div/div[3]/button[2]").click()  # Press on proceed on confirmation dialog


def check_sms_label():
    time.sleep(5)
    block_text = driver.find_element_by_xpath("//*[@id='SMS Consents']/th[1]/p").text
    assert block_text == 'SMS Consents'
    url = driver.current_url
    r = requests.get(url)
    status_code = r.status_code
    return status_code


def open_record_list():
    time.sleep(5)
    driver.find_elements_by_class_name("text-center")[4].click()  # Record List


def add_new_patient():
    time.sleep(5)
    driver.find_element_by_class_name("btn.btn-secondary.float-left.text-white").click()  # Add button


def edit_patient():
    time.sleep(5)
    driver.find_element_by_class_name("btn.btn-secondary.ml-2").click()  # Edit button
    time.sleep(5)
    driver.find_element_by_class_name("btn.btn-secondary.float-right.mt-1").click()  # Add address button
    time.sleep(5)
    middle_name = driver.find_element_by_id("middle-name")  # Middle-name input field
    middle_name.send_keys("auto")
    time.sleep(5)
    driver.find_element_by_class_name("btn.btn-secondary.ml-2").click()  # Submit button
    asrt = driver.find_element_by_class_name("list-group-item.list-group-item-danger").text
    return asrt


def press_cancel_on_confirmation():
    time.sleep(5)
    driver.find_elements_by_class_name("btn.btn-secondary.float-right")[1].click()  # Cancel button
    url = driver.current_url
    r = requests.get(url)
    status_code = r.status_code
    return status_code


def submit_new_patient():
    time.sleep(5)
    first_name = driver.find_element_by_id("first-name")  # First-name input field
    first_name.send_keys("auto")
    middle_name = driver.find_element_by_id("middle-name")  # Middle-name input field
    middle_name.send_keys("auto")
    last_name = driver.find_element_by_id("last-name")  # Last-name input field
    last_name.send_keys("auto")
    driver.find_element_by_id("enrollment-status").click()  # Set Enrollment Status
    driver.find_element_by_id("enrollment-method").click()  # Set Enrollment Method
    driver.find_element_by_class_name("mx-datepicker").click()  # Click on Enrollment Date
    driver.find_element_by_class_name("btn.btn-secondary.float-right").click()  # Submit button
    url = driver.current_url
    r = requests.get(url)
    status_code = r.status_code  # 200
    return status_code
