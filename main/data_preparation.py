import time
import json
import requests
from main import get_credentials

# Connection details
tm4j_token = get_credentials.get_tm4j_token()
base_url = get_credentials.get_base_url()

# URL list and data preparation
tc_url = f'{base_url}testcases'
pretty_time = time.strftime('%Y-%m-%dT%H:%M:%SZ')
time_for_cycle = time.asctime()

# URL list
base_url = get_credentials.get_base_url()
executions_url = f'{base_url}testexecutions'
cycle_url = f'{base_url}testcycles'


def tc_steps_count(test_case_key):
    status_headers = {'Content-Type': 'application/json', 'Authorization': f'Bearer {tm4j_token}'}
    steps_count = requests.get(f'{tc_url}/{test_case_key}/teststeps', headers=status_headers)
    return steps_count


def tc_steps_definition(tc_status):
    step_data = {
        "statusName": f"{tc_status}",
        "actualEndDate": f"{pretty_time}",
        "actualResult": "This STEP was verified by automation"
    }
    return step_data


def tc_status(project_key, test_case_key, test_cycle_key, status, body, execution_time):
    json = {
        "projectKey": f"{project_key}",
        "testCaseKey": f"{test_case_key}",
        "testCycleKey": f"{test_cycle_key}",
        "statusName": f"{status}",
        "testScriptResults": [
            body
        ],
        "actualEndDate": f"{pretty_time}",
        "executionTime": execution_time,
        "executedById": "377441B7-835D-4B08-B7F4-219E9E62C015",
        "assignedToId": "377441B7-835D-4B08-B7F4-219E9E62C015",
        "comment": "This TEST CASE was verified by automation"
    }
    return json


def create_test_cycle(project_key):
    cycle_body = {
        "projectKey": f"{project_key}",
        "name": f"[AUTOMATION][REGRESSION] {time_for_cycle}",
        "description": "This CYCLE was created by automation"
    }
    return cycle_body


def steps_definition(test_case_key, tc_status):
    total_steps = tc_steps_count(test_case_key).json()['total']
    get_steps = tc_steps_definition(tc_status)
    steps_multiplication = str(get_steps) * total_steps
    pretty_steps_body = str(steps_multiplication).replace('}{', '}, {')
    return pretty_steps_body


def posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status,
                                                 pretty_steps_body, execution_time):
    pre_output_body = tc_status(project_key, test_case_key, test_cycle_key, tc_status,
                                                 pretty_steps_body, execution_time)
    normalized_body = str(pre_output_body).replace('["', '[').replace('"]', ']').replace("'", '"')
    output_body = json.JSONDecoder().decode(normalized_body)
    status_headers = {'Content-Type': 'application/json', 'Authorization': f'Bearer {tm4j_token}'}
    requests.post(executions_url, headers=status_headers, json=output_body)