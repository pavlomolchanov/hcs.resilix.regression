import yaml

# Configuration
config_location = r'config/application.yaml'


def get_tm4j_token():
    with open(config_location) as file:
        configuration = yaml.load(file, Loader=yaml.FullLoader)
        tm4j_token = configuration.get('TM4J')['tm4j_token']
        return tm4j_token


def get_base_url():
    with open(config_location) as file:
        configuration = yaml.load(file, Loader=yaml.FullLoader)
        base_url = configuration.get('TM4J')['base_url']
        return base_url


def get_project_key():
    with open(config_location) as file:
        configuration = yaml.load(file, Loader=yaml.FullLoader)
        project_key = configuration.get('JIRA API')['jira_project_key']
        return project_key


def get_test_cycle_key():
    with open(config_location) as file:
        configuration = yaml.load(file, Loader=yaml.FullLoader)
        test_cycle_key = configuration.get('TM4J')['cycle_key']
        return test_cycle_key


def start_page():
    with open(config_location) as file:
        configuration = yaml.load(file, Loader=yaml.FullLoader)
        ce = configuration.get('ENVIRONMENT')['CURRENT ENVIRONMENT']
        start_link = configuration.get('ENVIRONMENT')[ce]['start_link']
        return start_link
