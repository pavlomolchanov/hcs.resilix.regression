import requests
from main import data_preparation
from main import get_credentials

# Connection details
project_key = get_credentials.get_project_key()
tm4j_token = get_credentials.get_tm4j_token()

# URL list
base_url = get_credentials.get_base_url()
executions_url = f'{base_url}testexecutions'
cycle_url = f'{base_url}testcycles'


def test_new_test_cycle():
    data = data_preparation.create_test_cycle(project_key)
    status_headers = {'Content-Type': 'application/json', 'Authorization': f'Bearer {tm4j_token}'}
    send = requests.post(cycle_url, headers=status_headers, json=data)
    test_cycle_key = send.json()['key']
    logs = open("tc_key.log", "w")
    logs.write(test_cycle_key)
    logs.close()
    return test_cycle_key
