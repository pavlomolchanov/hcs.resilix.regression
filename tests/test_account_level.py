import json
import time
import yaml
import requests
from main import data_preparation
from main import get_credentials
from main import test_create_cycle
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

# Connection details
project_key = get_credentials.get_project_key()
tm4j_token = get_credentials.get_tm4j_token()

# URL list
base_url = get_credentials.get_base_url()
executions_url = f'{base_url}testexecutions'
cycle_url = f'{base_url}testcycles'

# Test data
test_data = r'test_data/preparation.yaml'
configuration = yaml.load(open(test_data), Loader=yaml.FullLoader)
start_link = get_credentials.start_page()
default_user = configuration.get('DEFAULT DATA')['default_user']
default_password = configuration.get('DEFAULT DATA')['default_pass']

# Take Test Cycle Key
test_cycle_key = open("tc_key.log", "r").read()


def test_account_top_menu_bar():
    test_case_key = 'RES-T91'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['is_displayed']
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)
    driver.get(start_link)
    username = driver.find_element_by_id("user-email-address")
    password = driver.find_elements_by_class_name("form-control")[1]
    login_button = driver.find_element_by_id("user-")
    username.send_keys(default_user)
    password.send_keys(default_password)
    login_button.click()
    time.sleep(5)
    but = driver.find_element_by_class_name(
        "text-center.d-inline-flex.align-items-center.justify-content-center.picker-container.btn-account-logo")
    but.click()
    time.sleep(5)
    logo = driver.find_element_by_class_name("card.logo-small").is_displayed()
    if logo == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_account_tabs_menu():
    test_case_key = 'RES-T92'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['is_displayed']
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)
    driver.get(start_link)
    username = driver.find_element_by_id("user-email-address")
    password = driver.find_elements_by_class_name("form-control")[1]
    login_button = driver.find_element_by_id("user-")
    username.send_keys(default_user)
    password.send_keys(default_password)
    login_button.click()
    time.sleep(5)
    but = driver.find_element_by_class_name(
        "text-center.d-inline-flex.align-items-center.justify-content-center.picker-container.btn-account-logo")
    but.click()
    time.sleep(5)
    call_list = driver.find_element_by_class_name("nav.flex-column.nav-pills").is_displayed() == 'True'
    driver.find_element_by_class_name("text-center.nav-link.sharpCorners.navTextmain").click()
    time.sleep(5)
    logo_cl = driver.find_element_by_class_name("navbar").is_displayed()
    if logo_cl == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_primary_entity_search_fields():
    test_case_key = 'RES-T56'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['text']
    input_data = configuration.get(test_case_key)['search']
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)
    driver.get(start_link)
    username = driver.find_element_by_id("user-email-address")
    password = driver.find_elements_by_class_name("form-control")[1]
    login_button = driver.find_element_by_id("user-")
    username.send_keys(default_user)
    password.send_keys(default_password)
    login_button.click()
    time.sleep(5)
    but = driver.find_element_by_class_name(
        "text-center.d-inline-flex.align-items-center.justify-content-center.picker-container.btn-account-logo")
    but.click()
    time.sleep(5)
    driver.find_element_by_class_name("nav.flex-column.nav-pills").click()
    driver.find_element_by_class_name("btn.btn-outline-primary.text-center.navLeftBtn").click()
    time.sleep(2)
    driver.find_element_by_class_name("form-control").send_keys(input_data)
    driver.find_element_by_class_name("btn.btn-secondary.mt-3.ml-1").click()
    time.sleep(2)
    res = driver.find_element_by_class_name("text-muted").text
    if res == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_call_search_fields():
    test_case_key = 'RES-T57'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['tel_no']
    input_data = configuration.get(test_case_key)['tel_no']
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)
    driver.get(start_link)
    username = driver.find_element_by_id("user-email-address")
    password = driver.find_elements_by_class_name("form-control")[1]
    login_button = driver.find_element_by_id("user-")
    username.send_keys(default_user)
    password.send_keys(default_password)
    login_button.click()
    time.sleep(5)
    but = driver.find_element_by_class_name(
        "text-center.d-inline-flex.align-items-center.justify-content-center.picker-container.btn-account-logo")
    but.click()
    time.sleep(5)
    driver.find_element_by_class_name("nav.flex-column.nav-pills").click()
    driver.find_element_by_class_name("btn.btn-outline-primary.text-center.navLeftBtn").click()
    time.sleep(2)
    driver.find_element_by_class_name("form-control").send_keys(input_data)
    driver.find_element_by_class_name("btn.btn-secondary.mt-3.ml-1").click()
    time.sleep(2)
    res = driver.find_element_by_class_name("text-muted").text
    if res == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_my_calls_all_calls_dropdown_filter():
    test_case_key = 'RES-T59'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['text']
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)
    driver.get(start_link)
    username = driver.find_element_by_id("user-email-address")
    password = driver.find_elements_by_class_name("form-control")[1]
    login_button = driver.find_element_by_id("user-")
    username.send_keys(default_user)
    password.send_keys(default_password)
    login_button.click()
    time.sleep(5)
    but = driver.find_element_by_class_name(
        "text-center.d-inline-flex.align-items-center.justify-content-center.picker-container.btn-account-logo")
    but.click()
    time.sleep(5)
    driver.find_element_by_class_name("nav.flex-column.nav-pills").click()
    driver.find_element_by_class_name("form-control.float-right.mb-2").send_keys(Keys.ARROW_DOWN)
    time.sleep(2)
    button = Select(driver.find_element_by_class_name("form-control.float-right.mb-2"))
    button.select_by_visible_text('All Calls')
    time.sleep(2)
    aler = driver.find_element_by_class_name("col-md-2").text
    if aler == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_version_number():
    test_case_key = 'RES-T109'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['version']
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)
    driver.get(start_link)
    username = driver.find_element_by_id("user-email-address")
    password = driver.find_elements_by_class_name("form-control")[1]
    login_button = driver.find_element_by_id("user-")
    username.send_keys(default_user)
    password.send_keys(default_password)
    login_button.click()
    time.sleep(5)
    but = driver.find_element_by_class_name(
        "text-center.d-inline-flex.align-items-center.justify-content-center.picker-container.btn-account-logo")
    but.click()
    vers = driver.find_element_by_class_name("text-center.version").text
    if vers == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)
