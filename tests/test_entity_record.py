import time
import yaml
from main import data_preparation
from main import get_credentials
from main import actions

# Connection details
project_key = get_credentials.get_project_key()

# Test data
test_data = r'test_data/preparation.yaml'
configuration = yaml.load(open(test_data), Loader=yaml.FullLoader)

# Take Test Cycle Key
test_cycle_key = open("tc_key.log", "r").read()


def test_new_entity_form():
    test_case_key = 'RES-T87'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['status code']
    actions.log_in_default_user()
    actions.open_record_list()
    actions.add_new_patient()
    status_code = actions.submit_new_patient()
    if status_code == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_edit_profile_tab():
    test_case_key = 'RES-T356'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['assertion message']
    actions.log_in_default_user()
    actions.open_record_list()
    actions.select_first_patient()
    asrt = actions.edit_patient()
    if asrt == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_new_entity_form_fields_validation():
    test_case_key = 'RES-T114'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['status code']
    actions.log_in_default_user()
    actions.open_record_list()
    actions.add_new_patient()
    status_code = actions.press_cancel_on_confirmation()
    if status_code == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)