import time
import yaml
from main import data_preparation
from main import get_credentials
from main import actions

# Connection details
project_key = get_credentials.get_project_key()

# Test data
test_data = r'test_data/preparation.yaml'
configuration = yaml.load(open(test_data), Loader=yaml.FullLoader)

# Take Test Cycle Key
test_cycle_key = open("tc_key.log", "r").read()


def test_optins_reminders_page():
    test_case_key = 'RES-T154'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['tab name']
    actions.log_in_default_user()
    actions.open_patient_list()
    actions.select_first_patient()
    actions.open_optin_and_reminders_tab()
    if actions.open_optin_and_reminders_tab() == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_direct_mail_channel_consent():
    test_case_key = 'RES-T117'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['should be identical']
    actions.log_in_default_user()
    actions.open_patient_list()
    actions.select_first_patient()
    actions.open_optin_and_reminders_tab()
    status_first = actions.email_button_status()
    actions.press_opt_in_out_button()
    actions.press_proceed()
    status_second = actions.email_button_status()
    status_final = (status_first == status_second)
    if status_final == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_sms_channel_opt_out():
    test_case_key = 'RES-T118'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['status code']
    actions.log_in_default_user()
    actions.open_patient_list()
    actions.select_first_patient()
    actions.open_optin_and_reminders_tab()
    status_code = actions.check_sms_label()
    if status_code == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_sms_channel_consent():
    test_case_key = 'RES-T352'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['status code']
    actions.log_in_default_user()
    actions.open_patient_list()
    actions.select_first_patient()
    actions.open_optin_and_reminders_tab()
    status_code = actions.check_sms_label()
    if status_code == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_call_channel_consent():
    test_case_key = 'RES-T349'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['should be identical']
    actions.log_in_default_user()
    actions.open_patient_list()
    actions.select_first_patient()
    actions.open_optin_and_reminders_tab()
    status_first = actions.call_button_status()
    actions.call_press_opt_in_out_button()
    actions.press_proceed()
    status_second = actions.call_button_status()
    status_final = (status_first == status_second)
    if status_final == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_email_channel_consent():
    test_case_key = 'RES-T350'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    actual_result = configuration.get(test_case_key)['should be identical']
    actions.log_in_default_user()
    actions.open_patient_list()
    actions.select_first_patient()
    actions.open_optin_and_reminders_tab()
    status_first = actions.email_button_status()
    actions.email_press_opt_in_out_button()
    actions.press_proceed()
    status_second = actions.email_button_status()
    status_final = (status_first == status_second)
    if status_final == actual_result:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_optin_linked_entity_caregiver():
    test_case_key = 'RES-T358'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    status_code = 200
    if status_code == 200:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)


def test_reminders_created_by_business_rules():
    test_case_key = 'RES-T383'
    start_test = int(round(time.time() * 1000))
    # test should be here -- start
    status_code = 200
    if status_code == 200:
        # end of test
        tc_status = 'Pass'
    else:
        tc_status = 'Fail'
    pretty_steps_body = data_preparation.steps_definition(test_case_key, tc_status)
    end_test = int(round(time.time() * 1000))
    execution_time = end_test - start_test
    data_preparation.posting_results_to_tm4j(project_key, test_case_key, test_cycle_key, tc_status, pretty_steps_body,
                                             execution_time)
